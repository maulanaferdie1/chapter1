
// saya mengimport package prompt-sync sebagai inputan di terminal
import Prompt from "prompt-sync";

// saya jalankan method prompt nya di variabel prompt agar bisa digunakan nanti
const prompt = Prompt();


// list Function
const akarKuadrat = nilai => {
    // disini saya menggunakan method Math.sqrt() untuk mencari hasil akar kuadrat
    return Math.sqrt(nilai);
}

const luasPersegi = (panjang, lebar) => {
    return panjang * lebar;
};

const volumeKubus = sisi => {
    return sisi**3;
};

const volumeTabung = (jari2, tinggi) => {
    // karena phi dalam rumus berbeda jadi saya membuat jika tinggi habis dibagi 7 maka phi nya 22/7 dan jika tidak maka 3.14
    if(tinggi % 7 == 0){
        return 22/7 * jari2**2 * tinggi;
    } else {
        return 3.14 * jari2**2 * tinggi;
    }
}

// membuat variabel a dan b sebagai inputan yang akan dimasukkan nanti
let a,b;

// membuat perulangan agar kode program bisa diulang jika di inginkan
while(true){

// menu pilihan
console.log(`
    ============== SELAMAT DATANG DI KALKULATOR KITA ================
    ==============  SILAHKAN PILIH MENU DIBAWAH INI  ================
	
	>>- OPERASI SEDERHANA :             >>- OPERASI LANJUTAN :        
	    1 TAMBAH                            5 AKAR KUADRAT     
	    2 KURANG                            6 LUAS PERSEGI   
	    3 PERKALIAN                         7 VOLUME KUBUS
	    4 PEMBAGIAN                         8 VOLUME TABUNG    \n`)

    let pilihan = prompt(`Menu apa yang ingin anda pilih =  `)

    // memakai switch case sebagai tempat pilihan dan pengoperasian kalkulasi
    switch (pilihan) {
        case "1":
            a = Number(prompt("masukkan nilai pertama = "));
            b = Number(prompt("masukkan nilai kedua = "));
            console.log(`Hasil Penjumlahan anda adalah = ${a+b}`);
            break;
        case "2":
            a = Number(prompt("masukkan nilai pertama = "));
            b = Number(prompt("masukkan nilai kedua = "));
            console.log(`Hasil Pengurangan anda adalah = ${a-b}`);
        break;
            case "3":
            a = Number(prompt("masukkan nilai pertama = "));
            b = Number(prompt("masukkan nilai kedua = "));
            console.log(`Hasil Perkalian anda adalah = ${a*b}`);
            break;
        case "4":
            a = Number(prompt("masukkan nilai pertama = "));
            b = Number(prompt("masukkan nilai kedua = "));
            console.log(`Hasil Pembagian anda adalah = ${a/b}`);
            break;
        case "5":
            a = Number(prompt("masukkan nilai = "));
            console.log(`Akar kuadrat dari ${a} adalah = ${akarKuadrat(a)}`);
            break;
        case '6':
            a = Number(prompt("masukkan nilai panjang persegi = "));
            b = Number(prompt("masukkan nilai lebar persegi = "));
            console.log(`Luas perseginya adalah = ${luasPersegi(a,b)}`);
            break;
        case '7':
            a = Number(prompt("masukkan nilai sisi kubus = "));
            console.log(`Luas volume kubusnya adalah = ${volumeKubus(a)}`);
            break;
        case '8':
            a = Number(prompt("masukkan nilai jari-jari tabung = "));
            b = Number(prompt("masukkan nilai tinggi tabung = "));
            console.log(`Luas volume tabung adalah = ${volumeTabung(a,b)}`);
            break;
        default:
            console.log("Anda salah menginputkan menu");
            break;
    }

    // menu ulang
    console.log(`\nApakah anda ingin mengulang ?
    Tekan y untuk mengulang
    Tekan apapun untuk berhenti\n`);
    let ulang = prompt("inputan anda = ")

    // memakai if jika pilihan nya adalah y maka menu akan terus berulang jika tidak maka disini akan menutup dengan break 
    if(ulang != 'y'){
        // penutup jika tidak diulang
    console.log(`                                                     
   =============== TERIMA KASIH TELAH MENGUNJUNGI ==================
   ===============       KALKULATOR KITA        ====================
   *****************************************************************\n`)
        break;
    }
}